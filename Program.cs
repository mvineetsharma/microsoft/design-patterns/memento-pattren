﻿using System;

// Originator: The object whose state needs to be saved and restored
class Fruit
{
    public string Name { get; set; }
    public int Quantity { get; set; }

    public Fruit(string name, int quantity)
    {
        Name = name;
        Quantity = quantity;
    }

    // Create a Memento object to save the state
    public FruitMemento CreateMemento()
    {
        return new FruitMemento(Name, Quantity);
    }

    // Restore the state from a Memento object
    public void RestoreMemento(FruitMemento memento)
    {
        Name = memento.Name;
        Quantity = memento.Quantity;
    }

    public void Display()
    {
        Console.WriteLine($"Fruit: {Name}, Quantity: {Quantity}");
    }
}

// Memento: Represents the state of the Originator
class FruitMemento
{
    public string Name { get; }
    public int Quantity { get; }

    public FruitMemento(string name, int quantity)
    {
        Name = name;
        Quantity = quantity;
    }
}

// Caretaker: Responsible for storing and managing Memento objects
class FruitCaretaker
{
    public FruitMemento Memento { get; set; }
}

class Program
{
    static void Main(string[] args)
    {
        // Create a Fruit object
        var fruit = new Fruit("Apple", 10);
        fruit.Display();

        // Store the state in a Memento
        var caretaker = new FruitCaretaker();
        caretaker.Memento = fruit.CreateMemento();

        // Modify the state
        fruit.Name = "Banana";
        fruit.Quantity = 5;
        fruit.Display();

        // Restore the state from the Memento
        fruit.RestoreMemento(caretaker.Memento);
        fruit.Display();
    }
}
